import numpy as np

a = [1, 2, 3, 4]
b = [5, 6, 7, 8]

c = []

if(len(a) == len(b)):
    length  = len(a)
    for i in range(length):
        c.append(a[i] + b[i])

c = np.array(c).reshape(2, 2)

print(c)