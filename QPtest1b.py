def reduce_string(s):
    while True:
        reduced_str = ''
        i = 0

        while i < len(s):
            if i == len(s) - 1 or s[i] != s[i + 1]:
                reduced_str += s[i]
                i += 1
            else:
                i += 2

        if s == reduced_str:
            break

        s = reduced_str

    if reduced_str:
        return reduced_str
    else:
        return "Empty String"

# Test case
input_str = "aaabccddd"
output = reduce_string(input_str)
print(f"Output: {output}")
