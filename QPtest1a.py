def count_camel_case_words(s):
    word_count = 1  # Initialize count to 1 for the first word
    for char in s:
        if char.isupper():
            word_count += 1
    return word_count

# Test case
input_str = "AroundTheWorld"
output = count_camel_case_words(input_str)
print(f"Output: {output}")