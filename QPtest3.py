import re

def validate_driving_license(license_number):
    pattern = r'^[A-Z]{2}\d{2}\s(19|20)\d{2}\d{7}$'
    return bool(re.match(pattern, license_number))

# Test case
license_number = "TN09 20181563489"

if validate_driving_license(license_number):
    print("Valid Driving License")
else:
    print("Invalid Driving License")
