import pandas as pd

myDataset = {
    'Cars' : ['City', 'Civic', 'Verna', 'Creta', 'Ciaz', 'Vento', 'Rapid', 'Verna', 'Creta', 'Ciaz', 'Vento', 'Rapid'],
    'Company' : ['Honda', 'Honda', 'Hyundai', 'Hyundai', 'Maruti', 'Volkswagen', 'Skoda', 'Hyundai', 'Hyundai', 'Maruti', 'Volkswagen', 'Skoda'],
}

df = pd.DataFrame(myDataset)

honda_df = df[df['Company'] == 'Honda']

print(honda_df)
print( df['Company'].value_counts() )