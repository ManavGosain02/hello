import numpy as np

a = np.random.randint(1, 11, size=(2, 3))
b = np.random.randint(1, 11, size=(2, 3))

print(a)
print(b)

a = a.reshape(1, 6)
b = b.reshape(1, 6)

print(a)
print(b)

c = []

if (len(a) == len(b)):
    length = len(a)
    for i in range(length):
        c.append(a[i] + b[i])

c = np.array(c)
        
print(c)
print(c.reshape(2, 3))

print((a + b).reshape(2, 3))