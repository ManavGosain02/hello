import numpy as np

# Define two matrices
matrix1 = np.array([[1, 2], [3, 4]])
matrix2 = np.array([[5, 6], [7, 8]])

# Perform matrix multiplication using dot() function
result_dot = np.dot(matrix1, matrix2)
print("Matrix Multiplication using dot():")
print(result_dot)

# Perform matrix multiplication using @ operator (available in Python 3.5+)
result_at = matrix1 @ matrix2
print("\nMatrix Multiplication using @ operator:")
print(result_at)