def palindrome(s):
    char_count = {}  # Dictionary to store character frequencies

    # Count the frequencies of characters in the string
    for char in s:
        char_count[char] = char_count.get(char, 0) + 1

    odd_count = 0  # Counter for characters with odd frequencies

    # Count the number of characters with odd frequencies
    for count in char_count.values():
        if count % 2 != 0:
            odd_count += 1

    # If there are no characters with odd frequencies, the entire string can be rearranged into a palindrome
    if odd_count == 0:
        return len(s)
    else:
        return len(s) - odd_count + 1

# Test cases
print(palindrome("murali"))      # Output: 1
print(palindrome("malayalam"))   # Output: 9
