def count_anagrammatic_pairs(s):
    count = 0
    substrings = []

    # Generate all possible substrings
    for i in range(len(s)):
        for j in range(i + 1, len(s) + 1):
            substrings.append(s[i:j])

    # Check for anagrammatic pairs
    for i in range(len(substrings)):
        for j in range(i + 1, len(substrings)):
            if sorted(substrings[i]) == sorted(substrings[j]):
                count += 1

    return count

# Test case
input_str = "mom"
output = count_anagrammatic_pairs(input_str)
print(f"Output: {output}")